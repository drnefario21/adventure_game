import subprocess
import time
import random

# checking if the termcolor dependency is installed, if not, prompt the player to install it.
try:
    from termcolor import colored
except:
    print("You don't have the module termcolor installed.")
    print("Would you like to install it now? [Y/n] ", end="")
    usr_input = input().lower()

    if usr_input == "y" or usr_input == "yes" or usr_input == "":
        subprocess.run("pip3 install termcolor", shell=True)
        
        print("\n\nAll dependencies successfully installed.")
        time.sleep(1.5)
        
        from termcolor import colored
    else:
        print("You can install it by typing \"pip3 install -r requirements.txt\".")
        exit()

# These modules handle colored text.
from colorama import init

# Custom modules
import gamedata
from gamedata import clear
from gamedata import savefile

# colorama
init()

# detect the operating system
gamedata.detect_os()

# clear the screen
clear()

savefile = savefile("load")
Debug = savefile["other"]["Debug"]

playerLocation = savefile["player"]["last_location"]

def main():
    global playerLocation
    global Debug
    global savefile

    welcome = "Welcome!"
    x = welcome.center(80)
    print(colored(x, "magenta"))
    print("You are in a room. The room is relatively dark, but the lantern hanging from the ceiling emits some light.")
    print("Type \"?\" or \"help\" to open the help menu or \"commands\" to quickly see the command list.")

    while True:
        savefile = gamedata.savefile("load")
        playerLocation = savefile["player"]["last_location"]
        prompt_colour = savefile["other"]["prompt_colour"]
        header_colour = savefile["other"]["manual"]["header_colour"]
        location_colour = savefile["other"]["location_colour"]

        print(playerLocation, colored("> ", prompt_colour), end="")
        usr_input_str = input() # prompt
        usr_input = usr_input_str.split(" ") # creates a table of things the player entered divided by spaces

        if usr_input[0] in gamedata.game_commands or usr_input[0] in gamedata.core_commands:
            if usr_input[0] == "exit":
                exit()

            elif usr_input[0] == "clear":
                gamedata.clear()

            elif usr_input[0] == "help" or usr_input[0] == "?":
                gamedata.help()

            elif usr_input[0] == "commands":
                print(colored("Game commands", header_colour) + ":")

                for command in gamedata.game_commands:
                    if command == gamedata.game_commands[len(gamedata.game_commands) - 1]:
                        print(command)
                    else:
                        print(command, end=", ")

                print(colored("\nCore commands", header_colour) + ":")

                for command in gamedata.core_commands:
                    if command == gamedata.core_commands[len(gamedata.core_commands) - 1]:
                        print(command)
                    else:
                        print(command, end=", ")

            elif usr_input[0] == "go":
                try:
                    if playerLocation == "basement" and playerLocation == "outside":
                        if playerLocation == "basement":
                            print("You should go back through the trapdoor to move.")
                        else:
                            print("You should go back through the window to move.")

                    else:
                        if usr_input[1] != "back":
                            savefile["player"]["prev_location"] = playerLocation
                            gamedata.savefile(savefile, "save")

                        if usr_input[1] == "north":
                            if playerLocation != "north":
                                playerLocation = "north"
                                if savefile["room"]["carpet"]["moved"] == False:
                                    print("You went to the", colored("north", location_colour), "of the room. You see a carpet on the floor.")
                                else:
                                    print("You went to the", colored("north", location_colour), "of the room. There is a carpet you moved.")
                            else:
                                print("You are already at the", colored("north", location_colour), "of the room.")
                        elif usr_input[1] == "south":
                            if playerLocation != "south":
                                playerLocation = "south"
                                print("You went to the", colored("south", location_colour), "of the room. Nothing here.")
                            else:
                                print("You are already at the", colored("south", location_colour), "of the room.")
                        elif usr_input[1] == "west":
                            if playerLocation != "west":
                                playerLocation = "west"
                                print("You went to the", colored("west", location_colour), "of the room. You see a cabinet.")
                                gamedata.savefile(savefile, "save")
                            else:
                                print("You are already at the", colored("west", location_colour), "of the room.")
                        elif usr_input[1] == "east":
                            if playerLocation != "east":
                                playerLocation = "east" 
                                print("You went to the", colored("east", location_colour), "of the room. There is a window, fully closed. You can't look outside.")
                                print("\x1B[3m- Perhaps I can open it?\x1B[0m")
                            else:
                                print("You are already at the", colored("east", location_colour), "of the room")
                        elif usr_input[1] == "back":
                            if playerLocation != savefile["player"]["prev_location"]:
                                playerLocation = savefile["player"]["prev_location"]
                            else:
                                print("You are already in this location.")
                        else:
                            print(colored("error", "red") + f": {usr_input[1]} is an invalid location. See manual.")

                        savefile["player"]["last_location"] = playerLocation
                        gamedata.savefile(savefile, "save")

                except Exception as e:
                    print(colored("error", "red") + f": {usr_input[0]} requires an argument.")
                    if savefile["other"]["Debug"] == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "eat":
                try:
                    if usr_input[1] in ["lunch", "snack", "chicken"]:
                        if savefile["player"]["inventory"]["eat"][usr_input[1]] > 0:
                            savefile["player"]["inventory"]["eat"][usr_input[1]] -= 1
                            print(f"You ate a {usr_input[1]}.")
                        else:
                            print("You don't have enough of this item. Needed amount:", colored("1", "green"))
                        
                        gamedata.savefile(savefile, "save")
                    else:
                        print(colored("error", "red") + f": {usr_input[1]} is an invalid food type. See manual.")
                except Exception as e:
                    print(colored("error", "red") + f": {usr_input[0]} requires an argument.")
                    if Debug == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "drink":
                try:
                    if usr_input[1] in ["water"]:
                        if savefile["player"]["inventory"]["drink"][usr_input[1].lower()] > 0:
                            print(f"You drank a {usr_input[1].lower()}")
                        else:
                            print("You don't have enough of this item. Needed amount:", colored("1", "green"))
                    else:
                        print(colored("error", "red") + f": {usr_input[1]} is an invalid drink type. See manual.")
                except Exception as e:
                    print(colored("error", "red") + f": {usr_input[0]} requires an argument.")
                    if Debug == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "lift":
                try:
                    if usr_input[1] == "carpet":
                        if playerLocation == "north":
                            if savefile["room"]["carpet"]["lifted"] == False:
                                savefile = gamedata.savefile("load")

                                if savefile["room"]["carpet"]["tries"] == 0:
                                    print("\x1B[3m- Why is it so heavy? Is it glued to the floor?\x1B[0m")
                                    time.sleep(1.5)
                                    print("You couldn't lift the carpet because it was glued to the floor... Maybe try again?")
                                    savefile["room"]["carpet"]["tries"] += 1

                                elif savefile["room"]["carpet"]["tries"] == 1:
                                    print("\x1B[3m- Hmm...\x1B[0m")
                                    time.sleep(1.5)
                                    savefile["room"]["carpet"]["tries"] += 1

                                elif savefile["room"]["carpet"]["tries"] == 2:
                                    print("\x1B[3m- Yes!\x1B[0m")

                                    time.sleep(1.5)
                                    print("You lifted the carpet.")
                                    print("There is a trap door.")

                                    savefile["room"]["carpet"]["lifted"] = True
                                    savefile["room"]["carpet"]["tries"] += 1

                                else:
                                    print("testing")

                                gamedata.savefile(savefile, "save")
                            else:
                                print("The carpet is already lifted.")
                        else:
                            print("\x1B[3mHm. I can't see a carpet here...\x1B[0m")
                    else:
                        print(colored("error", "red") + f": {usr_input[1]} is an invalid argument. See manual.")
                except Exception as e:
                    print(colored("error", "red") + f": {usr_input[0]} requires an argument.")
                    if Debug == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "inventory":
                savefile = gamedata.savefile("load")
                print("Water: ", end="")
                print(savefile["player"]["inventory"]["drink"]["water"])
                print("Chicken: ", end="")
                print(savefile["player"]["inventory"]["eat"]["chicken"])
                print("Lunch: ", end="")
                print(savefile["player"]["inventory"]["eat"]["lunch"])
                print("Snack: ", end="")
                print(savefile["player"]["inventory"]["eat"]["snack"])
                print("Gun: ", end="")
                print(savefile["player"]["inventory"]["items"]["gun"])

            elif usr_input[0] == "loot":
                savefile = gamedata.savefile("load")
                try:
                    if usr_input[1] == "cabinet":
                        if savefile["player"]["last_location"] == "west":
                            if savefile["room"]["cabinet"]["opened"] == True:
                                if savefile["room"]["cabinet"]["looted"] == False:
                                    savefile["player"]["inventory"]["drink"]["water"] += 1
                                    savefile["player"]["inventory"]["eat"]["snack"] += 2
                                    savefile["player"]["inventory"]["items"]["gun"] += 1
                                    savefile["room"]["cabinet"]["looted"] = True
                                    gamedata.savefile(savefile, "save")
                                    
                                    print("Looted cabinet, got:")
                                    print("+1 water")
                                    print("+2 snacks")
                                    print("+1 gun")
                                else:
                                    print("You have already looted this cabinet.")
                            else:
                                print("You should first open the cabinet.")
                        else:
                            print("There isn't a cabinet here.")
                    else:
                        print(colored("Error: ", "red") + f"{usr_input[1]}: invalid argument")
                except Exception as e:
                    print(colored("error:", "red"), f"{usr_input[0]} requires an argument.")
                    if savefile["other"]["Debug"] == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "open":
                savefile = gamedata.savefile("load")
                try:
                    if usr_input[1] == "cabinet":
                        if savefile["player"]["last_location"] == "west":
                            if savefile["room"]["cabinet"]["opened"] == False:
                                savefile["room"]["cabinet"]["opened"] = True

                                if savefile["room"]["cabinet"]["looted"] == False:
                                    print("You opened the cabinet. There is a gun, 2 snacks, a bottle of water and a key.")
                                    print("Take them? [Y/n] ", end="")
                                    usr_input = input().lower()

                                    if usr_input == "" or usr_input == "yes" or usr_input == "y":
                                        savefile["player"]["inventory"]["drink"]["water"] += 1
                                        savefile["player"]["inventory"]["eat"]["snack"] += 2
                                        savefile["player"]["inventory"]["items"]["gun"] += 1
                                        savefile["player"]["inventory"]["items"]["has_trapdoor_key"] = True
                                        savefile["room"]["cabinet"]["looted"] = True
                                        gamedata.savefile(savefile, "save")
                                    else:
                                        pass
                                else:
                                    print("You have already looted this cabinet.")
                                
                                gamedata.savefile(savefile, "save")
                            else:
                                print("You already opened this cabinet.")
                        else:
                            print("There isn't a cabinet here.")
                    
                    elif usr_input[1] == "window":
                        savefile = gamedata.savefile("load")

                        if savefile["player"]["last_location"] == "east":
                            if savefile["room"]["window"]["opened"] == False:
                                if savefile["room"]["window"]["tries"] == 0 or savefile["room"]["window"]["tries"] == 1:
                                    savefile["room"]["window"]["tries"] += 1
                                    tries = savefile["room"]["window"]["tries"]

                                    if tries == 1:
                                        print("The window is hard to open, but you managed to open it a little bit.")
                                        print("But in order to enter it, you should open it fully.")
                                    elif tries == 2:
                                        print("You opened the window.")
                                        savefile["room"]["window"]["opened"] = True
                                    else:
                                        print("Exceeded tries")
                                    
                                    gamedata.savefile(savefile, "save")
                                else:
                                    print("Exceeded tries.")
                            else:
                                print("The window is already opened.")
                        else:
                            print("There isn't a window in this part of the room.")

                    elif usr_input[1] == "trapdoor":
                        savefile = gamedata.savefile("load")
                        
                        if savefile["player"]["last_location"] == "north":
                            if savefile["room"]["carpet"]["lifted"] == True:
                                if savefile["room"]["carpet"]["moved"] == True:
                                    if savefile["player"]["inventory"]["items"]["has_trapdoor_key"] == True:
                                        if savefile["room"]["trapdoor"]["locked"] == True:
                                            savefile["room"]["trapdoor"]["locked"] = False
                                            gamedata.savefile(savefile, "save")

                                            if savefile["room"]["trapdoor"]["opened"] == False:
                                                print("You opened the trapdoor.")

                                                savefile["room"]["trapdoor"]["opened"] = True
                                                gamedata.savefile(savefile, "save")
                                            else:
                                                print("You've already opened this trapdoor.")
                                        else:
                                            print("You've already opened this trapdoor.")
                                    else:
                                        print("\x1B[3mIt seems to be locked... I need a key to open it.\x1B[0m")
                                else:
                                    print("The carpet should first be moved.")
                            else:
                                print("There isn't a trapdoor.")
                        else:
                            print("There isn't a trapdoor nearby.")                            

                    else:
                        print(colored("Error:", "red"), f"{usr_input[0]}: invalid argument.")

                except Exception as e:
                    print(colored("error:", "red"), f"{usr_input[0]} requires an argument.")
                    if savefile["other"]["Debug"] == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "move":
                try:
                    if usr_input[1] == "carpet":
                        if savefile["room"]["carpet"]["lifted"] == True:
                            if savefile["room"]["carpet"]["moved"] == False:
                                print("You moved the carpet.")
                                savefile["room"]["carpet"]["moved"] = True
                                gamedata.savefile(savefile, "save")
                            else:
                                print("The carpet is already moved.")
                        else:
                            print("You should lift the carpet first.")
                    else:
                        print(colored("Error", "red") + f": {usr_input[1]}: invalid argument. See manual.")
                except Exception as e:
                    print(colored("error:", "red"), f"{usr_input[0]} requires an argument.")
                    if savefile["other"]["Debug"] == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "enter":
                savefile = gamedata.savefile("load")
                try:
                    if usr_input[1] == "window":
                        if playerLocation == "east" or playerLocation == "outside":
                            if savefile["room"]["window"]["opened"] == True:
                                if playerLocation == "east":
                                    savefile["player"]["last_location"] = "outside"

                                    if savefile["room"]["window"]["has_ever_entered"] == False:
                                        print("You entered the window. You see a creature outside, possibly hostile. It's better if you stay in the room.")

                                        savefile["room"]["window"]["has_ever_entered"] = True
                                        gamedata.savefile(savefile, "save")
                                    else:
                                        random_phrase = random.randint(0, len(gamedata.hostile_area_phrases) - 1)
                                        print(f"\x1B[3m{gamedata.hostile_area_phrases[random_phrase]}\x1B[0m")

                                elif playerLocation == "outside":
                                    savefile["player"]["last_location"] = "east"
                                else:
                                    print("There isn't a window nearby.")
                                
                                gamedata.savefile(savefile, "save")
                            else:
                                print("The window should be opened first.")

                        else:
                            print("There isn't a window nearby.")

                    elif usr_input[1] == "trapdoor":
                        if playerLocation == "north" or playerLocation == "basement":
                            if savefile["room"]["trapdoor"]["locked"] == False:
                                if savefile["room"]["trapdoor"]["opened"] == True:
                                    if playerLocation == "north":
                                        savefile["player"]["last_location"] = "basement"

                                        if savefile["room"]["trapdoor"]["has_ever_entered"] == False:
                                            print("You enter the basement. There is nothing but jars and canned food.")
                                            
                                            savefile["room"]["trapdoor"]["has_ever_entered"] = True
                                            gamedata.savefile(savefile, "save")
                                        else:
                                            pass
                                    elif playerLocation == "basement":
                                        savefile["player"]["last_location"] = "north"
                                    else:
                                        print("There isn't a trapdoor nearby.")
                                    
                                    gamedata.savefile(savefile, "save")
                                else:
                                    print("You should open the trapdoro first.")
                            else:
                                print("The trapdoor is locked.")
                        else:
                            print("There isn't a trapdoor nearby.")

                    else:
                        print(colored("error", "red") + f": {usr_input[1]} is an invalid argument. See manual.")
                except Exception as e:
                    print(colored("error:", "red"), f"{usr_input[0]} requires an argument.")
                    if savefile["other"]["Debug"] == True:
                        print(colored("Error:", "red"), end="")
                        print(colored(e, "red"))

            elif usr_input[0] == "look":
                print("You looked around.")

                if playerLocation == "north":
                    if savefile["room"]["carpet"]["moved"]:
                        print("There is a carpet and a trapdoor.")
                    else:
                        print("There is a carpet.")
                elif playerLocation == "south":
                    print("There is nothing.")
                elif playerLocation == "west":
                    if savefile["room"]["cabinet"]["opened"] == True:
                        if savefile["room"]["cabinet"]["looted"] == True:
                            print("There is a looted cabinet.")
                        else:
                            print("There is an unlooted cabinet.")
                    else:
                        print("There is a closed cabinet.")
                elif playerLocation == "east":
                    if savefile["room"]["window"]["tries"] == 1:
                        print("There is a slightly open window.")
                        print("You can't see anything because it's dark outside.")
                    else:
                        if savefile["room"]["window"]["opened"] == True:
                            print("There is an open window.")
                        else:
                            print("There is a closed window.")
                elif playerLocation == "basement":
                    print("There is nothing except for jars and canned food.")
                elif playerLocation == "outside":
                    print("There is a hostile creature that will kill you on sight.")
                else:
                    print("? Incorrect location ?")

            elif usr_input[0] == "use":
                print("wip")

            elif usr_input[0] == "debug":
                if Debug == True:
                    Debug = False
                    print(colored("Debugging disabled.", "yellow"))
                else:
                    Debug = True
                    print(colored("Debugging enabled.", "yellow"))

                savefile["other"]["Debug"] = Debug

                gamedata.savefile(savefile, "save")

            elif usr_input[0] == "settings":
                gamedata.settings()

        else:
            print(colored("error", "red") + f": {usr_input[0]}: command not found. See manual.")

main()
