# adventure_game

A text-based Python-written adventure game.

## Installation

<code>pip3 install -r requirements.txt</code>

## Usage

<code>python3 main.py</code>
