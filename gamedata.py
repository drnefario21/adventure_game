# This is the file where most of the game's variables and functions are stored.
import os
import platform
import json
import time
from colorama import init
from termcolor import colored

# Custom modules
import manual_handler

init() #colorama init function

manual0 = ""
manual1 = ""
manual2 = ""

# core
clear_command = "clear"
game_commands = ["eat", "drink", "use", "open", "loot", "go", "lift", "move", "inventory","settings", "enter", "look"]
core_commands = ["clear", "exit", "help", "?", "commands", "debug"]


# game
items = ["water", "chicken", "lunch", "snack", "gun"] # items
hostile_area_phrases = ["I should be cautious here.",
                        "This area is hostile.",
                        "I will be killed if I'm spotted here.",
                        "I hope I won't be noticed here."]


# variables
sv = []
with open("save.json", "r") as f:
    sv = json.load(f)

prompt_colour = sv["other"]["prompt_colour"]

# functions
def detect_os():
    operating_system = platform.system()
    
    if operating_system == "Linux" or operating_system == "Darwin":
        clear = "clear"
        print(f"os: {operating_system}")
    elif operating_system == "Windows":
        clear = "cls"
        print(f"os: {operating_system}")
    
    try:
        os.system(clear_command)
    except:
        print("Perhaps you're using a shell different from Windows, Bash (GNU/Linux, MacOS)?")
        print("Please enter the command to clear the output of your terminal below (case-sensitive).", end='')
        clear = input()

def clear():
    os.system(clear_command)

def help():
    manual_page = 0

    while True:
        clear()

        manual_handler.manual(manual_page)

        print(colored("\nexit", "green") + "/" + colored("next", "green") + "/" + colored("prev", "green") + " - exit; next/previous page")
        print("\nPage", colored(manual_page + 1, "green"), "of", colored("5", "green"))

        usr_input = input(colored(f"help $ ", "yellow"))

        if usr_input == "next":
            if manual_page >= 0 and manual_page < 4:
                manual_page += 1
        elif usr_input == "prev" or usr_input == "previous":
            if manual_page <= 5 and manual_page > 0:
                manual_page -= 1
        elif usr_input == "q" or usr_input == "exit":
            break


def savefile(savetable, operation="load"):
    if operation == "load":
        with open("save.json", "r") as f:
            return json.load(f)
    elif operation == "save":
        with open("save.json", "w") as f:
            json.dump(savetable, f, indent=4, sort_keys=True)


def settings():

    while True:
        clear()
        sv = savefile("load")
        prompt_colour = sv["other"]["prompt_colour"]

        print("In this menu you can change your settings.")
        print("Enter \"exit\" or \"q\" to go back to the game.")
        print("In a menu, press ENTER to go back.")
        print("1. Player")
        print("2. Game progress")
        print("3. Game configuration")
        usr_input = input(colored("[settings]$ ", prompt_colour))

        if usr_input == "1":
            print("Nothing here.")
            time.sleep(1)
        elif usr_input == "2":
            print("1. Reset game progress & configuration")
            print("2. Change game progress (cheats)")
            usr_input = input(colored("[settings", prompt_colour) + " game progress" + colored("]$ ", prompt_colour))

            if usr_input == "1":
                print("Are you sure you want to reset your game progress and configuration?")
                print("It " + colored("\x1B[3mcannot\x1B[0m", "red") + " be undone.")
                usr_input = input("[N/y] ").lower()

                if usr_input == "y" or usr_input == "yes":
                    with open("default/save.json", "r") as f:
                        f = json.load(f)
                        with open("save.json", "w") as f0:
                            json.dump(f, f0, indent=4, sort_keys=True)
                    print("Game progress has been reset.")
                    time.sleep(1)
                else:
                    pass

            elif usr_input == "2":
                print("Enter an item name and then the amount you would like to be added to your inventory.")
                print("You can find the item list in the manual page or type \"items\" here to access it.")
                while True:
                    print(colored("[cheat menu]$ ", prompt_colour), end="")
                    usr_input = input()
                    usr_input = usr_input.split(" ")

                    print(usr_input[0])
                    if usr_input[0] == "q" or usr_input[0] == "exit":
                        break
                    elif usr_input[0] == "items":
                        for item in items:
                            print(item)
                    elif usr_input[0] in items:
                        try:
                            sv = savefile("load")
                            sv["player"]["inventory"]["drink"][usr_input[0]] += int(usr_input[1])
                            print(f"{usr_input[1]} {usr_input[0]}(s) was/were added to your inventory.")
                            savefile(sv, "save")
                        except Exception as e:
                            sv = savefile("load")
                            print(f"Please specify the amount of {usr_input[0]} you'd like to add to your inventory.")
                            if sv["other"]["Debug"] == True:
                                print(colored("Error: " + str(e), "red"))
                    else:
                        print(colored("error:", "red"), "command/item not found.")

        elif usr_input == "3":
            print("1. Change colours")
            print("2. Change prompt style")
            usr_input = input(colored("[settings", prompt_colour) + " game config" + colored("]$ ", prompt_colour))

            if usr_input == "1":
                operation = 0

                print("1. Prompt colour")
                print("2. Manual colours (colours in the help manual)")
                print("3. Locations colour (e.g. North, South)")
                usr_input = input(colored("[settings", prompt_colour) + " change colours" + colored("]$ ", prompt_colour))
                
                if usr_input == "1":
                    operation = 1
                elif usr_input == "2":
                    print("1. Argument colour (e.g. <type> or <item>)")
                    print("2. Header colour (e.g. Core commands)")
                    usr_input = input(colored(" --> ", prompt_colour))

                    if usr_input == "1":
                        operation = 2
                    elif usr_input == "2":
                        operation = 3
                    else:
                        print(colored("Error: ", "red") + "invalid option.")
                        return
                elif usr_input == "3":
                    operation = 4
                else:
                    print(colored("Error: ", "red") + "invalid input.")


                print("Choose one of the colours (1-9):")
                colours = ["Grey", "Red", "Green", "Yellow", "Blue", "Magenta", "Cyan", "White"]
                for colour in colours:
                    print(str(colours.index(colour)+1) + ".", colour)


                try:
                    usr_input = int(input(colored(" --> ", prompt_colour)))

                    if int(usr_input) >= 1 and int(usr_input) <= 8:
                        if operation == 1:
                            sv["other"]["prompt_colour"] = colours[int(usr_input) - 1].lower()
                        elif operation == 2:
                            sv["other"]["manual"]["arg_colour"] = colours[int(usr_input) - 1].lower()
                        elif operation == 3:
                            sv["other"]["manual"]["header_colour"] = colours[int(usr_input) - 1].lower()
                        elif operation == 4:
                            sv["other"]["location_colour"] = colours[int(usr_input) - 1].lower()

                        savefile(sv, "save")
                    else:
                        print(colored("Error: ", "red") + "input out of range.")

                except Exception as e:
                    print("Please enter only integers.")

                    if savefile("load")["other"]["Debug"] == True:
                        print(colored("Error: ", "red"), end="")
                        print(colored(e, "red"))
                    
                    input("Press ENTER to continue.")

        elif usr_input == "q" or usr_input == "exit":
            break
