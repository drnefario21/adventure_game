# created this python file to make the gamedata.py less messy
# the colours of the manual are customizable
import gamedata
from colorama import init
from termcolor import colored


def manual(number):
    # Colours of the manual. See https://pypi.org/project/termcolor/ for more colours
    sv = gamedata.savefile("load")
    header_colour = sv["other"]["manual"]["header_colour"]
    arg_colour = sv["other"]["manual"]["arg_colour"]
    location_colour = sv["other"]["location_colour"]

    print(colored("Help manual", header_colour).center(80))

    if number == 0:
        print(colored("Core commands", header_colour).center(80))
        print(colored("\nclear", "green"), "- clear the screen")
        print(colored("exit", "green"), "- exit the game")
        print(colored("help", "green") + ",", colored("?", "green"), "- open the help manual")
        print(colored("commands", "green"), "- get the commands list")
        print(colored("settings", "green"), "- change game settings")
        print(colored("debug", "green"), "- toggle debug mode on/off (shows exceptions from try/except)")
    elif number == 1:
        print(colored("Game commands", header_colour).center(80))
        # eat
        print(colored("\neat", "green") + " <" + colored("type", arg_colour) + "> - eat something")
        print("    ├── lunch")
        print("    ├── snack")
        print("    └── chicken")
        # drink
        print(colored("drink", "green") + " <" + colored("type", arg_colour) + "> - drink something")
        print("    └── water")
        # go
        print(colored("go", "green") + " <" + colored("location", arg_colour) + "> - go to a location")
        print("    ├── north")
        print("    ├── west")
        print("    ├── south")
        print("    ├── east")
        print("    └── back")
        # lift
        print(colored("lift", "green") + " <" + colored("item", arg_colour) + "> - lift something")
        print("    └── carpet")
        # enter
        print(colored("enter", "green") + " <" + colored("argument", arg_colour) + "> - enter something")
        print("    ├── trapdoor")
        print("    └── window")
        # move
        print(colored("move", "green") + " <" + colored("item", arg_colour) + "> - move something")
        # use
        print(colored("use", "green") + " <" + colored("item", arg_colour) + "> - use an item")
        # take
        print(colored("take", "green") + " <" + colored("item", arg_colour) + "> - take an item")
        # look
        print(colored("look", "green") + " - print surroundings")
    elif number == 2:
        print(colored(f"Gameplay (pg {number-1}/3)", header_colour).center(80))
        print("This section of the manual is divided into three sections.\n")
        # section 1
        print(colored("Section 1: spawn", header_colour).center(80))
        print("You are spawned in the south of the room and there is nothing nearby.")
        print("There are four locations in the room:")
        print("    ├── the " + colored("south", location_colour) + ", which is the zone you spawn in;")
        print("    ├── the " + colored("north", location_colour) + ", which has a carpet on the floor;")
        print("    ├── the " + colored("west", location_colour) + ", with a cabinet;")
        print("    ├── the " + colored("east", location_colour) + ", with a window, fully closed.")
        print("    └── the " + colored("outside", location_colour) + ", everything is dark, but a hostile")
        print("        creature is nearby. You can go outside by entering the window.\n")
        print("You can interact with these objects by typing commands. To view them,")
        print("go to the " + colored("2nd", "green")+ " page of the manual or type " + colored("commands", "green") + ".")
        print("Some objects have items in them or other things, i.e. a trapdoor.")
    elif number == 3:
        print(colored(f"Gameplay (pg {number-1}/3)", header_colour).center(80))
        print("This section of the manual is divided into three sections.\n")
        # section 2
        print(colored("Section 2: items", header_colour).center(80))
        print("You can find items in different locations. After picking up an item/looting something, the item")
        print("goes to the player's inventory. You can view inventory with the " + colored("inventory", "green") + " command.")
        print("Items have use. For example, a food/drink-type item can be used to eat/drink. To do that, type " + colored("eat", "green"))
        print("or " + colored("drink", "green") + " respectively.")
        print("Other items, such as guns, can be used to kill a creature (not yet implemented, I will probably")
        print("run out of time; perhaps I will continue developing this game for fun after submitting it.)")
    elif number == 4:
        print(colored(f"Gameplay (pg {number-1}/3)", header_colour).center(80))
        print("This section of the manual is divided into three sections.\n")
        # section 3
        print(colored("Section 3: ?", header_colour).center(80))
        print("Nothing here yet.")
